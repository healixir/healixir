Healixir is the only NAD+ supplement in the world with the purest and most potent NAD+ available delivered in a slow time release capsule for the highest absorption in a supplement. Healixir will allow other supplements and therapies work better with a surplus of NAD in the body.

Website: https://healixir.co/
